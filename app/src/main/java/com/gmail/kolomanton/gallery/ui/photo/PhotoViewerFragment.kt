package com.gmail.kolomanton.gallery.ui.photo

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v4.view.ViewPager
import android.util.TypedValue
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gmail.kolomanton.gallery.R
import com.gmail.kolomanton.gallery.di.DI
import com.gmail.kolomanton.gallery.entity.Photo
import com.gmail.kolomanton.gallery.extention.toast
import com.gmail.kolomanton.gallery.presenter.gallery.GalleryPresenter
import com.gmail.kolomanton.gallery.presenter.gallery.GalleryView
import com.gmail.kolomanton.gallery.presenter.photo.PhotoViewerPresenter
import com.gmail.kolomanton.gallery.presenter.photo.PhotoViewerView
import com.gmail.kolomanton.gallery.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_photo_viewer.*
import toothpick.Toothpick
import toothpick.config.Module


class PhotoViewerFragment : BaseFragment(), PhotoViewerView {

    override val layoutRes = R.layout.fragment_photo_viewer

    override fun onBackPressed() = presenter.onBackPressed()

    @InjectPresenter
    lateinit var presenter: PhotoViewerPresenter

    @ProvidePresenter
    fun providePresenter(): PhotoViewerPresenter {
        val scopeName = "GalleryScope_${hashCode()}"
        val scope = Toothpick.openScopes(DI.SERVER_SCOPE, scopeName)
        scope.installModules(object : Module() {
            init {
                bind(PhotoViewerPresenter.Filter::class.java)
                        .toInstance(
                                PhotoViewerPresenter
                                        .Filter(arguments?.getString(ARG_PHOTO_ID))
                        )
            }
        })

        return scope.getInstance(PhotoViewerPresenter::class.java)
                .also { Toothpick.closeScope(scopeName) }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.apply {
            setNavigationOnClickListener { onBackPressed() }
            setTitleTextColor(Color.WHITE)

            val typedValue = TypedValue()
            val theme = context!!.theme
            theme.resolveAttribute(R.attr.homeAsUpIndicator, typedValue, true)

            val backIcon = DrawableCompat.wrap(ContextCompat.getDrawable(context!!,
                    typedValue.resourceId)!!)
            DrawableCompat.setTint(backIcon, Color.WHITE)
            navigationIcon = backIcon
        }
    }

    override fun onStart() {
        super.onStart()
        setStatusBarDark(true)
    }

    override fun onStop() {
        setStatusBarDark(false)
        super.onStop()
    }

    private fun setStatusBarDark(isDark: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isDark) {
                activity!!.window!!.statusBarColor = ContextCompat.getColor(context!!, android.R.color.black)
                activity!!.window!!.decorView.systemUiVisibility = 0
            } else {
                activity!!.window!!.statusBarColor = ContextCompat.getColor(context!!, android.R.color.white)
                activity!!.window!!.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }
    }

    override fun showUploadedPhotoList(photos: List<Photo>, index: Int) {
        view_pager.adapter = PhotoViewerAdapter(activity!!.supportFragmentManager, photos) { presenter.getPhoto(it) }
        view_pager.currentItem = index
        toolbar.title = getString(R.string.of, index + 1, photos.size)
        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                view_pager.setPageTransformer(true, DepthPageTransformer())
            }

            override fun onPageSelected(position: Int) {
                toolbar.title = getString(R.string.of, position + 1, photos.size)
                presenter.onPageSelected(position)
            }
        })
    }

    override fun setPage(position: Int) {
        view_pager.currentItem = position
    }

    override fun showToast(message: String) {
        context?.toast(message)
    }

    companion object {
        private const val ARG_PHOTO_ID = "arg_photo_id"

        fun newInstance(photoId: String? = null) =
                PhotoViewerFragment().apply {
                    if (photoId != null)
                        arguments = Bundle().apply { putString(ARG_PHOTO_ID, photoId) }
                }
    }
}