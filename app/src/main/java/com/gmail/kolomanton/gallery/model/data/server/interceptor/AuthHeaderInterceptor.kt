package com.gmail.kolomanton.gallery.model.data.server.interceptor

import com.gmail.kolomanton.gallery.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class AuthHeaderInterceptor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder().addHeader("Authorization", "Client-ID ${BuildConfig.ACCESS_KEY}").build()
        return chain.proceed(request)
    }
}