package com.gmail.kolomanton.gallery.model.data.server

import com.gmail.kolomanton.gallery.entity.Photo
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface UnsplashApi {

    @GET("photos")
    fun getPhotos(
            @Query("page") page: Int,
            @Query("per_page") itemsOnPage: Int
    ): Single<List<Photo>>
}