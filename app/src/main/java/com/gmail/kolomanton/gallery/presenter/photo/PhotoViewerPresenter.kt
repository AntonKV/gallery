package com.gmail.kolomanton.gallery.presenter.photo

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gmail.kolomanton.gallery.entity.Photo
import com.gmail.kolomanton.gallery.model.interactor.GalleryInteractor
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class PhotoViewerPresenter @Inject constructor(
        initFilter: Filter,
        private val router: Router,
        private val interactor: GalleryInteractor
) : MvpPresenter<PhotoViewerView>() {

    data class Filter(var photoId: String?)

    private var filter = initFilter

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        interactor.getUploadedPhotoList()
                .subscribe({
                    viewState.showUploadedPhotoList(it,
                            it.indexOf(it.find { it.id == filter.photoId }))
                }, {
                    viewState.showToast(it.message!!)
                })
    }

    fun onBackPressed() = router.exit()

    fun deletePhoto(currentPhoto: Int): Boolean {
        viewState.showToast("Вы уверены что хотите удалить фото №$currentPhoto?")
        return true
    }

    fun onPageSelected(position: Int) {
        viewState.setPage(position)
    }

    fun getPhoto(id: String?) = interactor.getPhoto(id)
}