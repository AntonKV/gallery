package com.gmail.kolomanton.gallery.ui.gallery.adapter

import android.support.v4.util.SparseArrayCompat
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.gmail.kolomanton.gallery.entity.AdapterConst
import com.gmail.kolomanton.gallery.entity.Photo
import com.gmail.kolomanton.gallery.ui.gallery.adapter.delegates.GalleryDelegateAdapter
import com.gmail.kolomanton.gallery.ui.global.DiffCallback
import com.gmail.kolomanton.gallery.ui.gallery.adapter.delegates.ProgressDelegateAdapter
import com.gmail.kolomanton.gallery.ui.gallery.adapter.items.ProgressItem
import com.gmail.kolomanton.gallery.ui.global.adapter.ViewType
import com.gmail.kolomanton.gallery.ui.global.adapter.ViewTypeDelegateAdapter

class GalleryAdapter(
        clickListener: (Photo) -> Unit,
        private val nextListener: () -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: MutableList<ViewType> = mutableListOf()
    private var delegateAdapter = SparseArrayCompat<ViewTypeDelegateAdapter>()

    init {
        delegateAdapter.put(AdapterConst.Gallery.PHOTO, GalleryDelegateAdapter(clickListener))
        delegateAdapter.put(AdapterConst.Gallery.LOADER, ProgressDelegateAdapter())
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
            delegateAdapter.get(getItemViewType(position))
                    .onBindViewHolder(holder, items[position], items.size)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            delegateAdapter.get(viewType).onCreateViewHolder(parent)

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int) = items[position].getViewType()

    fun setData(photos: List<Photo>) {
        val oldData = items.toList()
        val progress = isProgress()

        items.clear()
        items.addAll(photos)
        if (progress)
            items.add(ProgressItem())

        DiffUtil
                .calculateDiff(DiffCallback(items, oldData), false)
                .dispatchUpdatesTo(this)
    }

    fun showProgress(isVisible: Boolean) {
        val oldData = items.toList()
        val currentProgress = isProgress()

        if (isVisible && !currentProgress) {
            items.add(ProgressItem())
            notifyItemInserted(items.lastIndex)
        } else if (!isVisible && currentProgress) {
            items.remove(items.last())
            notifyItemRemoved(oldData.lastIndex)
        }
    }

    private fun isProgress() = items.isNotEmpty() && items.last() is ProgressItem

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any?>) {
        super.onBindViewHolder(holder, position, payloads)

        if (position == items.size - 10) nextListener()
    }
}