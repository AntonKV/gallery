package com.gmail.kolomanton.gallery.ui.photo

import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.gmail.kolomanton.gallery.R
import com.gmail.kolomanton.gallery.entity.Photo
import com.gmail.kolomanton.gallery.model.system.GlideApp
import com.gmail.kolomanton.gallery.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_photo.*

class PhotoFragment : BaseFragment() {
    override val layoutRes = R.layout.fragment_photo

    override fun onBackPressed() {}

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val photo = getPhoto(arguments?.getString(ARG_PHOTO_ID))
        if (photo != null) {
            if (photo_view != null)
                GlideApp.with(context!!)
                        .load(photo.urls.regular)
                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                        .placeholder(R.color.color_black_opacity_70)
                        .fitCenter()
                        .into(photo_view)

            author?.text = photo.user.getUrl()
            author?.movementMethod = LinkMovementMethod.getInstance()
        }
    }

    companion object {
        private const val ARG_PHOTO_ID = "arg_photo_id"

        private lateinit var getPhoto: (String?) -> Photo?

        fun newInstance(id: String? = null, getPhoto: (String?) -> Photo?) =
                PhotoFragment().apply {
                    if (id != null)
                        arguments = Bundle().apply { putString(ARG_PHOTO_ID, id) }
                    this@Companion.getPhoto = getPhoto
                }
    }
}