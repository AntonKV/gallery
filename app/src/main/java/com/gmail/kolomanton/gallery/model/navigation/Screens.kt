package com.gmail.kolomanton.gallery.model.navigation

object Screens {
    const val GALLERY_SCREEN = "gallery screen"
    const val PHOTO_SCREEN = "photo screen"

}