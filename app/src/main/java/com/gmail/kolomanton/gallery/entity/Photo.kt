package com.gmail.kolomanton.gallery.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.text.Html
import android.text.Spanned
import com.gmail.kolomanton.gallery.ui.global.adapter.ViewType
import org.jetbrains.annotations.NotNull

@Entity(tableName = "gallery_table")
data class Photo(
        @PrimaryKey @NotNull @ColumnInfo(name = "id") val id: String,
        @Embedded @NotNull val urls: Urls,
        @Embedded @NotNull val user: User
) : ViewType {
    override fun getViewType() = AdapterConst.Gallery.PHOTO
}

data class Urls(
        val small: String,
        val thumb: String,
        val raw: String,
        val regular: String,
        val full: String
)

data class User(
        @ColumnInfo(name = "user_id") val id: String,
        @ColumnInfo(name = "user_username") val username: String,
        @ColumnInfo(name = "user_name") val name: String
) {
    fun getUrl(): Spanned? {
        return Html.fromHtml(
                "Photo by <a href=\"https://unsplash.com/@$username?utm_source=gallery&utm_medium=referral\">$name</a> " +
                        "on <a href=\"https://unsplash.com/?utm_source=gallery&utm_medium=referral\">Unsplash.com</a>")
    }
}

