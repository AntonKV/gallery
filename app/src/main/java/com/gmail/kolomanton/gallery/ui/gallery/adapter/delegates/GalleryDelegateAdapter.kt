package com.gmail.kolomanton.gallery.ui.gallery.adapter.delegates

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.gmail.kolomanton.gallery.R
import com.gmail.kolomanton.gallery.entity.Photo
import com.gmail.kolomanton.gallery.extention.inflate
import com.gmail.kolomanton.gallery.model.system.GlideApp
import com.gmail.kolomanton.gallery.ui.global.adapter.ViewType
import com.gmail.kolomanton.gallery.ui.global.adapter.ViewTypeDelegateAdapter
import kotlinx.android.synthetic.main.item_photo.view.*

class GalleryDelegateAdapter(
        private val clickListener: (Photo) -> Unit
) : ViewTypeDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder = GalleryItemViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType?, size: Int) {
        holder as GalleryItemViewHolder
        holder.bind(item as Photo)
    }

    inner class GalleryItemViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            parent.inflate(R.layout.item_photo)) {
        fun bind(item: Photo) {
            with(itemView) {
                author.text = item.user.name
                GlideApp.with(context)
                        .load(item.urls.small)
                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                        .placeholder(R.color.image_placeholder)
                        .centerCrop()
                        .into(photos)
                setOnClickListener { clickListener(item) }
            }
        }
    }
}