package com.gmail.kolomanton.gallery.presenter.launch

import com.arellomobile.mvp.InjectViewState
import com.gmail.kolomanton.gallery.model.navigation.Screens
import com.gmail.kolomanton.gallery.presenter.global.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class LaunchPresenter @Inject constructor(
        private val router: Router
) : BasePresenter<LaunchView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        router.newRootScreen(Screens.GALLERY_SCREEN)
    }

    fun onBackPressed() = router.finishChain()
}