package com.gmail.kolomanton.gallery.presenter.gallery

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.gmail.kolomanton.gallery.entity.Photo

interface GalleryView: MvpView{
    fun showRefreshProgress(show: Boolean)
    fun showEmptyProgress(show: Boolean)
    fun showPageProgress(show: Boolean)
    fun showEmptyView(show: Boolean)
    fun showEmptyError(show: Boolean, message: String?)
    fun showPhotos(show: Boolean, photos: List<Photo>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)
}