package com.gmail.kolomanton.gallery.ui.global.adapter

/**
 * Created by Anton Kolomin on 15-Jun-18.
 */
interface ViewType {
    fun getViewType(): Int
}