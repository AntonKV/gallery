package com.gmail.kolomanton.gallery.entity

object AdapterConst {
    object Gallery {
        const val PHOTO = 1
        const val LOADER = 2
    }
}