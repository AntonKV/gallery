package com.gmail.kolomanton.gallery.ui.global.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.gmail.kolomanton.gallery.ui.global.adapter.ViewType

/**
 * Created by Anton Kolomin on 15-Jun-18
 */
interface ViewTypeDelegateAdapter {
    fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

    fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType?, size: Int)
}