package com.gmail.kolomanton.gallery.ui.gallery.adapter.delegates

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.gmail.kolomanton.gallery.R
import com.gmail.kolomanton.gallery.extention.inflate
import com.gmail.kolomanton.gallery.ui.gallery.adapter.items.ProgressItem
import com.gmail.kolomanton.gallery.ui.global.adapter.ViewType
import com.gmail.kolomanton.gallery.ui.global.adapter.ViewTypeDelegateAdapter

class ProgressDelegateAdapter : ViewTypeDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder = ProgressItemViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType?, size: Int) {
        holder as ProgressItemViewHolder
        holder.bind(item as ProgressItem)
    }

    inner class ProgressItemViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            parent.inflate(R.layout.item_progress)) {
        fun bind(data: ProgressItem) {}
    }
}