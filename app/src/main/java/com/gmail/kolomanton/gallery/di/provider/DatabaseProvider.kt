package com.gmail.kolomanton.gallery.di.provider

import android.arch.persistence.room.Room
import android.content.Context
import com.gmail.kolomanton.gallery.model.data.database.GalleryDatabase
import javax.inject.Inject
import javax.inject.Provider

class DatabaseProvider @Inject constructor(
        private val context: Context
) : Provider<GalleryDatabase> {

    override fun get() = Room
            .databaseBuilder(
                    context,
                    GalleryDatabase::class.java,
                    "gallery.db"
            ).build()
}