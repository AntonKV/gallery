package com.gmail.kolomanton.gallery.ui.launch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gmail.kolomanton.gallery.R
import com.gmail.kolomanton.gallery.di.DI
import com.gmail.kolomanton.gallery.model.navigation.Screens
import com.gmail.kolomanton.gallery.presenter.launch.LaunchPresenter
import com.gmail.kolomanton.gallery.presenter.launch.LaunchView
import com.gmail.kolomanton.gallery.ui.gallery.GalleryFragment
import com.gmail.kolomanton.gallery.ui.global.BaseActivity
import com.gmail.kolomanton.gallery.ui.global.BaseFragment
import com.gmail.kolomanton.gallery.ui.photo.PhotoViewerFragment
import ru.terrakok.cicerone.android.SupportAppNavigator
import toothpick.Toothpick

class MainActivity : BaseActivity(), LaunchView {

    override val layoutRes = R.layout.activity_main

    private val currentFragment
        get() = supportFragmentManager.findFragmentById(R.id.mainContainer) as BaseFragment?

    @InjectPresenter
    lateinit var presenter: LaunchPresenter

    @ProvidePresenter
    fun providePresenter(): LaunchPresenter {
        return Toothpick
                .openScope(DI.APP_SCOPE)
                .getInstance(LaunchPresenter::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Toothpick.openScopes(DI.APP_SCOPE).apply {
            Toothpick.inject(this@MainActivity, this)
        }

        super.onCreate(savedInstanceState)
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed() ?: presenter.onBackPressed()
    }

    override val navigator = object : SupportAppNavigator(this, R.id.mainContainer) {

        override fun createActivityIntent(context: Context, screenKey: String?, data: Any?): Intent? = null

        override fun createFragment(screenKey: String?, data: Any?): Fragment? = when (screenKey) {
            Screens.GALLERY_SCREEN -> GalleryFragment()
            Screens.PHOTO_SCREEN -> PhotoViewerFragment.newInstance(data as String)
            else -> null
        }
    }
}
