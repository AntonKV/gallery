package com.gmail.kolomanton.gallery.model.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.gmail.kolomanton.gallery.entity.Photo
import com.gmail.kolomanton.gallery.model.data.database.dao.GalleryDao

@Database(entities = [
    Photo::class
], version = 1)
abstract class GalleryDatabase : RoomDatabase() {
    abstract fun galleryDao(): GalleryDao
}