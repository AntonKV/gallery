package com.gmail.kolomanton.gallery.model.repository

import com.gmail.kolomanton.gallery.entity.Photo
import com.gmail.kolomanton.gallery.model.data.database.GalleryDatabase
import com.gmail.kolomanton.gallery.model.data.server.UnsplashApi
import com.gmail.kolomanton.gallery.model.system.SchedulersProvider
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class GalleryRepository @Inject constructor(
        private val api: UnsplashApi,
        private val schedulers: SchedulersProvider,
        private val galleryDatabase: GalleryDatabase
) {
    fun getPhotosPageFromBD() =
            galleryDatabase.galleryDao().getAll()
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())

    fun getPhotosPageFromApi(page: Int, itemsOnPage: Int = 20) =
            api.getPhotos(page, itemsOnPage)
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())

    fun savePhotos(photos: List<Photo>): Disposable = Observable
            .fromCallable { galleryDatabase.galleryDao().updateAll(photos) }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe()

}