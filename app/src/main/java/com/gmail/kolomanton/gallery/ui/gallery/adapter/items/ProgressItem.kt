package com.gmail.kolomanton.gallery.ui.gallery.adapter.items

import com.gmail.kolomanton.gallery.entity.AdapterConst
import com.gmail.kolomanton.gallery.ui.global.adapter.ViewType

class ProgressItem : ViewType {
    override fun getViewType() = AdapterConst.Gallery.LOADER
}