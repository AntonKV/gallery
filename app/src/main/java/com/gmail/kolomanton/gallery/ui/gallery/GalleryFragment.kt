package com.gmail.kolomanton.gallery.ui.gallery

import android.content.Context
import android.graphics.Point
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.WindowManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gmail.kolomanton.gallery.R
import com.gmail.kolomanton.gallery.di.DI
import com.gmail.kolomanton.gallery.entity.AdapterConst
import com.gmail.kolomanton.gallery.entity.Photo
import com.gmail.kolomanton.gallery.extention.visible
import com.gmail.kolomanton.gallery.presenter.gallery.GalleryPresenter
import com.gmail.kolomanton.gallery.presenter.gallery.GalleryView
import com.gmail.kolomanton.gallery.ui.gallery.adapter.GalleryAdapter
import com.gmail.kolomanton.gallery.ui.global.BaseFragment
import com.gmail.kolomanton.gallery.ui.global.ZeroViewHolder
import kotlinx.android.synthetic.main.fragment_gallery.*
import kotlinx.android.synthetic.main.include_toolbar.view.*
import kotlinx.android.synthetic.main.layout_base_list.*
import kotlinx.android.synthetic.main.layout_zero.*
import toothpick.Toothpick

class GalleryFragment : BaseFragment(), GalleryView {
    override val layoutRes = R.layout.fragment_gallery

    private var zeroViewHolder: ZeroViewHolder? = null

    private val galleryAdapter: GalleryAdapter by lazy {
        GalleryAdapter(
                { presenter.onPhotoClick(it) },
                { presenter.loadNextIssuesPage() }
        )
    }

    private val GRID_COLUMN_COUNT = 3

    @InjectPresenter
    lateinit var presenter: GalleryPresenter

    @ProvidePresenter
    fun providePresenter(): GalleryPresenter =
            Toothpick
                    .openScope(DI.SERVER_SCOPE)
                    .getInstance(GalleryPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        include_toolbar.toolbar.apply {
            title = getString(R.string.unsplash)
            navigationIcon = null
        }

        recyclerView.apply {
            val gridLayoutManager = object : GridLayoutManager(context, GRID_COLUMN_COUNT) {
                override fun getExtraLayoutSpace(state: RecyclerView.State?): Int {
                    return getScreenHeight(context!!)
                }

                fun getScreenHeight(context: Context): Int {
                    val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
                    val size = Point()
                    wm.defaultDisplay.getSize(size)
                    return size.y
                }
            }
            gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when (galleryAdapter.getItemViewType(position)) {
                        AdapterConst.Gallery.LOADER -> GRID_COLUMN_COUNT
                        else -> return when (position % 6) {
                            5 -> 3
                            3 -> 2
                            else -> 1
                        }
                    }
                }
            }
            layoutManager = gridLayoutManager
            itemAnimator = DefaultItemAnimator()
            setHasFixedSize(true)
            adapter = this@GalleryFragment.galleryAdapter
        }

        swipeToRefresh.setOnRefreshListener { presenter.refreshPhotos() }
        zeroViewHolder = ZeroViewHolder(zeroLayout) { presenter.refreshPhotos() }
    }

    override fun showRefreshProgress(show: Boolean) {
        swipeToRefresh.post { swipeToRefresh.isRefreshing = show }
    }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView.visible(show)

        swipeToRefresh.visible(!show)
        swipeToRefresh.post { swipeToRefresh.isRefreshing = false }
    }

    override fun showPageProgress(show: Boolean) {
        recyclerView.post { galleryAdapter.showProgress(show) }
    }

    override fun showEmptyView(show: Boolean) {
        if (show)
            zeroViewHolder?.showEmptyData()
        else
            zeroViewHolder?.hide()
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        if (show)
            zeroViewHolder?.showEmptyError(message)
        else
            zeroViewHolder?.hide()
    }

    override fun showPhotos(show: Boolean, photos: List<Photo>) {
        recyclerView.visible(show)
        recyclerView.post { galleryAdapter.setData(photos) }
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }
}