package com.gmail.kolomanton.gallery.di.module

import com.gmail.kolomanton.gallery.di.provider.ApiProvider
import com.gmail.kolomanton.gallery.di.provider.OkHttpClientProvider
import com.gmail.kolomanton.gallery.model.data.server.UnsplashApi
import com.gmail.kolomanton.gallery.model.interactor.GalleryInteractor
import com.gmail.kolomanton.gallery.model.system.AppSchedulers
import com.gmail.kolomanton.gallery.model.system.SchedulersProvider
import com.gmail.kolomanton.gallery.presenter.gallery.GalleryPresenter
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import toothpick.config.Module

class ServerModule : Module() {
    init {
        //Network
        bind(Gson::class.java).toInstance(GsonBuilder().create())
        bind(OkHttpClient::class.java).toProvider(OkHttpClientProvider::class.java).providesSingletonInScope()
        bind(SchedulersProvider::class.java).toInstance(AppSchedulers())
        bind(UnsplashApi::class.java).toProvider(ApiProvider::class.java).providesSingletonInScope()

        bind(GalleryInteractor::class.java).singletonInScope()
        bind(GalleryPresenter::class.java).singletonInScope()
    }
}