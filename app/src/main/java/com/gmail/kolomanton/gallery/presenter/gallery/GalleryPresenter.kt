package com.gmail.kolomanton.gallery.presenter.gallery

import com.arellomobile.mvp.InjectViewState
import com.gmail.kolomanton.gallery.entity.Photo
import com.gmail.kolomanton.gallery.model.interactor.GalleryInteractor
import com.gmail.kolomanton.gallery.model.navigation.Screens
import com.gmail.kolomanton.gallery.presenter.global.BasePresenter
import com.gmail.kolomanton.gallery.presenter.global.Paginator
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class GalleryPresenter @Inject constructor(
        private val interactor: GalleryInteractor,
        private val router: Router
) : BasePresenter<GalleryView>() {
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        refreshPhotos()
    }

    private val viewController = object : Paginator.ViewController<Photo> {
        override fun showEmptyProgress(show: Boolean) {
            viewState.showEmptyProgress(show)
        }

        override fun showEmptyError(show: Boolean, error: Throwable?) {
            viewState.showEmptyError(show, error?.message)
        }

        override fun showErrorMessage(error: Throwable) {
            viewState.showMessage(error.message!!)
        }

        override fun showEmptyView(show: Boolean) {
            viewState.showEmptyView(show)
        }

        override fun showData(show: Boolean, data: List<Photo>) {
            viewState.showPhotos(show, data)
        }

        override fun showRefreshProgress(show: Boolean) {
            viewState.showRefreshProgress(show)
        }

        override fun showPageProgress(show: Boolean) {
            viewState.showPageProgress(show)
        }
    }

    fun refreshPhotos() = interactor.getFreshData(viewController)

    fun loadNextIssuesPage() = interactor.loadNewPage(viewController)

    fun onPhotoClick(photo: Photo) {
        router.navigateTo(Screens.PHOTO_SCREEN, photo.id)
    }
}