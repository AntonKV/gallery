package com.gmail.kolomanton.gallery.model.interactor

import com.gmail.kolomanton.gallery.entity.Photo
import com.gmail.kolomanton.gallery.model.repository.GalleryRepository
import com.gmail.kolomanton.gallery.presenter.global.Paginator
import io.reactivex.Single
import javax.inject.Inject

class GalleryInteractor @Inject constructor(
        private val repository: GalleryRepository
) {
    private val paginator = Paginator(
            { getPhotos(it) },
            { getCachePhotos() }
    )

    fun getPhotos(page: Int) = repository.getPhotosPageFromApi(page).doOnSuccess {
        if (page == 1)
            repository.savePhotos(it)
    }

    fun getCachePhotos() = repository.getPhotosPageFromBD()

    fun getFreshData(viewController: Paginator.ViewController<Photo>) {
        paginator.refresh(viewController)
    }

    fun loadNewPage(viewController: Paginator.ViewController<Photo>) {
        paginator.loadNewPage(viewController)
    }

    fun getUploadedPhotoList(): Single<List<Photo>> = Single.just(paginator.getLoadedData())

    fun getPhoto(id: String?): Photo? = paginator.getLoadedData().find { id == it.id }
}