package com.gmail.kolomanton.gallery.di.module

import android.content.Context
import com.gmail.kolomanton.gallery.di.provider.DatabaseProvider
import com.gmail.kolomanton.gallery.model.data.database.GalleryDatabase
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module

class AppModule(context: Context) : Module() {
    init {
        //Global
        bind(Context::class.java).toInstance(context)

        //Navigation
        val cicerone = Cicerone.create(Router())
        bind(Router::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)

        //Database
        bind(GalleryDatabase::class.java).toProvider(DatabaseProvider::class.java).providesSingletonInScope()
    }
}