package com.gmail.kolomanton.gallery.presenter.photo

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.gmail.kolomanton.gallery.entity.Photo

interface PhotoViewerView : MvpView {
    @StateStrategyType(value = SkipStrategy::class)
    fun showToast(message: String)

    fun showUploadedPhotoList(photos: List<Photo>, index: Int)
    fun setPage(position: Int)
}