package com.gmail.kolomanton.gallery.presenter.global

import com.gmail.kolomanton.gallery.entity.Photo
import io.reactivex.Single
import io.reactivex.disposables.Disposable

class Paginator<T>(
        private val requestFactory: (Int) -> Single<List<T>>,
        private val cacheFactory: () -> Single<List<T>>
) {

    interface ViewController<T> {
        fun showEmptyProgress(show: Boolean)
        fun showEmptyError(show: Boolean, error: Throwable? = null)
        fun showEmptyView(show: Boolean)
        fun showData(show: Boolean, data: List<T> = emptyList())
        fun showErrorMessage(error: Throwable)
        fun showRefreshProgress(show: Boolean)
        fun showPageProgress(show: Boolean)
    }

    private val FIRST_PAGE = 1

    private var viewController: ViewController<T>? = null
    private var currentState: State<T> = EMPTY()
    private var currentPage = 0
    private val currentData = mutableListOf<T>()
    private var disposable: Disposable? = null

    fun refresh(viewController: ViewController<T>) {
        this@Paginator.viewController = viewController
        currentState.refresh()
    }

    fun loadNewPage(viewController: ViewController<T>) {
        this@Paginator.viewController = viewController
        currentState.loadNewPage()
    }

    private fun loadPage(page: Int) {
        disposable = requestFactory.invoke(page)
                .subscribe(
                        { currentState.newData(it) },
                        { currentState.fail(it) }
                )
    }

    private fun showCache() {
        disposable = cacheFactory.invoke()
                .subscribe(
                        { currentState.cacheData(it) },
                        { currentState.fail(it) }
                )
    }

    fun getLoadedData(): List<T> = currentData

    private interface State<T> {
        fun restart() {}
        fun refresh() {}
        fun loadNewPage() {}
        fun release() {}
        fun cacheData(data: List<T>) {}
        fun newData(data: List<T>) {}
        fun fail(error: Throwable) {}
    }

    private inner class EMPTY : State<T> {

        override fun refresh() {
            currentState = EMPTY_PROGRESS()
            viewController?.showEmptyProgress(true)
            showCache()
            loadPage(FIRST_PAGE)
        }

        override fun release() {
            currentState = RELEASED()
            disposable?.dispose()
        }
    }

    private inner class EMPTY_PROGRESS : State<T> {

        override fun restart() {
            loadPage(FIRST_PAGE)
        }

        override fun cacheData(data: List<T>) {
            if (data.isNotEmpty()) {
                currentData.clear()
                currentData.addAll(data)
                currentPage = FIRST_PAGE
                viewController?.showData(true, currentData)
                viewController?.showEmptyProgress(false)
            }
        }

        override fun newData(data: List<T>) {
            if (data.isNotEmpty()) {
                currentState = DATA()
                currentData.clear()
                currentData.addAll(data)
                currentPage = FIRST_PAGE
                viewController?.showData(true, currentData)
                viewController?.showEmptyProgress(false)
            } else {
                currentState = EMPTY_DATA()
                viewController?.showEmptyProgress(false)
                viewController?.showEmptyView(true)
            }
        }

        override fun fail(error: Throwable) {
            currentState = EMPTY_ERROR()
            viewController?.showEmptyProgress(false)
            viewController?.showEmptyError(true, error)
        }

        override fun release() {
            currentState = RELEASED()
            disposable?.dispose()
        }
    }

    private inner class EMPTY_ERROR : State<T> {

        override fun restart() {
            currentState = EMPTY_PROGRESS()
            viewController?.showEmptyError(false)
            viewController?.showEmptyProgress(true)
            loadPage(FIRST_PAGE)
        }

        override fun refresh() {
            currentState = EMPTY_PROGRESS()
            viewController?.showEmptyError(false)
            viewController?.showEmptyProgress(true)
            loadPage(FIRST_PAGE)
        }

        override fun release() {
            currentState = RELEASED()
            disposable?.dispose()
        }
    }

    private inner class EMPTY_DATA : State<T> {

        override fun restart() {
            currentState = EMPTY_PROGRESS()
            viewController?.showEmptyView(false)
            viewController?.showEmptyProgress(true)
            loadPage(FIRST_PAGE)
        }

        override fun refresh() {
            currentState = EMPTY_PROGRESS()
            viewController?.showEmptyView(false)
            viewController?.showEmptyProgress(true)
            loadPage(FIRST_PAGE)
        }

        override fun release() {
            currentState = RELEASED()
            disposable?.dispose()
        }
    }

    private inner class DATA : State<T> {

        override fun restart() {
            currentState = EMPTY_PROGRESS()
            viewController?.showData(false)
            viewController?.showEmptyProgress(true)
            loadPage(FIRST_PAGE)
        }

        override fun refresh() {
            currentState = REFRESH()
            viewController?.showRefreshProgress(true)
            loadPage(FIRST_PAGE)
        }

        override fun loadNewPage() {
            currentState = PAGE_PROGRESS()
            viewController?.showPageProgress(true)
            loadPage(currentPage + 1)
        }

        override fun release() {
            currentState = RELEASED()
            disposable?.dispose()
        }

        override fun newData(data: List<T>) {
            if (data.isNotEmpty()) {
                currentData.clear()
                currentData.addAll(data)
                currentPage = FIRST_PAGE
                viewController?.showData(true, currentData)
                viewController?.showEmptyProgress(false)
            }
        }
    }

    private inner class REFRESH : State<T> {

        override fun restart() {
            currentState = EMPTY_PROGRESS()
            viewController?.showData(false)
            viewController?.showRefreshProgress(false)
            viewController?.showEmptyProgress(true)
            loadPage(FIRST_PAGE)
        }

        override fun newData(data: List<T>) {
            if (data.isNotEmpty()) {
                currentState = DATA()
                currentData.clear()
                currentData.addAll(data)
                currentPage = FIRST_PAGE
                viewController?.showRefreshProgress(false)
                viewController?.showData(true, currentData)
            } else {
                currentState = EMPTY_DATA()
                currentData.clear()
                viewController?.showData(false)
                viewController?.showRefreshProgress(false)
                viewController?.showEmptyView(true)
            }
        }

        override fun fail(error: Throwable) {
            currentState = DATA()
            viewController?.showRefreshProgress(false)
            viewController?.showErrorMessage(error)
        }

        override fun release() {
            currentState = RELEASED()
            disposable?.dispose()
        }
    }

    private inner class PAGE_PROGRESS : State<T> {

        override fun restart() {
            currentState = EMPTY_PROGRESS()
            viewController?.showData(false)
            viewController?.showPageProgress(false)
            viewController?.showEmptyProgress(true)
            loadPage(FIRST_PAGE)
        }

        override fun newData(data: List<T>) {
            if (data.isNotEmpty()) {
                currentState = DATA()
                currentData.addAll(data)
                currentPage++
                viewController?.showPageProgress(false)
                viewController?.showData(true, currentData)
            } else {
                currentState = ALL_DATA()
                viewController?.showPageProgress(false)
            }
        }

        override fun refresh() {
            currentState = REFRESH()
            viewController?.showPageProgress(false)
            viewController?.showRefreshProgress(true)
            loadPage(FIRST_PAGE)
        }

        override fun fail(error: Throwable) {
            currentState = DATA()
            viewController?.showPageProgress(false)
            viewController?.showErrorMessage(error)
        }

        override fun release() {
            currentState = RELEASED()
            disposable?.dispose()
        }
    }

    private inner class ALL_DATA : State<T> {

        override fun restart() {
            currentState = EMPTY_PROGRESS()
            viewController?.showData(false)
            viewController?.showEmptyProgress(true)
            loadPage(FIRST_PAGE)
        }

        override fun refresh() {
            currentState = REFRESH()
            viewController?.showRefreshProgress(true)
            loadPage(FIRST_PAGE)
        }

        override fun release() {
            currentState = RELEASED()
            disposable?.dispose()
        }
    }

    private inner class RELEASED : State<T>
}