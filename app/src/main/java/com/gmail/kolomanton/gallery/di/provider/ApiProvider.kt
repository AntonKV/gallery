package com.gmail.kolomanton.gallery.di.provider

import com.gmail.kolomanton.gallery.BuildConfig
import com.gmail.kolomanton.gallery.model.data.server.UnsplashApi
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Provider

class ApiProvider @Inject constructor(
        private val okHttpClient: OkHttpClient,
        private val gson: Gson
) : Provider<UnsplashApi> {

    override fun get() =
            Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .baseUrl(BuildConfig.UNSPLASH_API)
                    .build()
                    .create(UnsplashApi::class.java)
}