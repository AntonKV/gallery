package com.gmail.kolomanton.gallery.model.data.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.gmail.kolomanton.gallery.entity.Photo
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
abstract class GalleryDao {
    @Transaction
    open fun updateAll(photos: List<Photo>) {
        deleteAll()
        insertAll(photos)
    }

    @Query("SELECT * from gallery_table")
    abstract fun getAll(): Single<List<Photo>>

    @Insert(onConflict = REPLACE)
    abstract fun insertAll(photos: List<Photo>)

    @Query("DELETE from gallery_table")
    abstract fun deleteAll()
}