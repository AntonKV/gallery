package com.gmail.kolomanton.gallery

import com.gmail.kolomanton.gallery.model.system.SchedulersProvider
import io.reactivex.schedulers.Schedulers

/**
 * Created by Anton Kolomin on 20-Jun-18.
 */
class TestSchedulers : SchedulersProvider {
    override fun ui() = Schedulers.trampoline()
    override fun computation() = Schedulers.trampoline()
    override fun trampoline() = Schedulers.trampoline()
    override fun newThread() = Schedulers.trampoline()
    override fun io() = Schedulers.trampoline()
}