package com.gmail.kolomanton.gallery.model.interactor

import com.gmail.kolomanton.gallery.entity.Photo
import com.gmail.kolomanton.gallery.model.repository.GalleryRepository
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class GalleryInteractorTest {

    private lateinit var interactor: GalleryInteractor
    private lateinit var repository: GalleryRepository

    private val page = 0

    @Before
    fun setUp() {
        repository = mock()
        interactor = GalleryInteractor(repository)
    }

    @Test
    fun getPhotos() {
        whenever(repository.getPhotosPageFromApi(any(), any())).thenReturn(getMockGallery())

        val observer = interactor.getPhotos(page).test()
        observer.awaitTerminalEvent()

        verify(repository).getPhotosPageFromApi(eq(page), any())
        observer
                .assertValueCount(1)
                .assertNoErrors()
        assert(observer.values()[0].size == 3)
    }

    private fun getMockGallery() = Single.just(
            listOf(mock<Photo>(), mock<Photo>(), mock<Photo>())
    )
}